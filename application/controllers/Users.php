<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('user_m');
    }


    public function index()
	{
		if(!user_rules('admin')) {
        	redirect('/','location');
        }
		$data['title'] = $this->lang->line('users_list');

		$select = array('id', 'username', 'email','user_group', 'active');
		$data['users'] = $this->user_m->select_from($select,'users');

		$this->data['content'] = $this->load->view('back/parts/users_admin.php',$data,TRUE);
		$this->load->view('back/index',$this->data);

	}

	public function add_user()
	{
		if(!user_rules('admin')) {
        	redirect('/','location');
        }
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
			        array(
			                'field' => 'username',
			                'label' => $this->lang->line('username'),
			                'rules' => 'trim|required|is_unique[users.username]|required|min_length[3]|max_length[12]'
			        ),
			        array(
			                'field' => 'password',
			                'label' => $this->lang->line('password'),
			                'rules' => 'trim|required|matches[password_confirm]|sha1'
			        ),
			        array(
			                'field' => 'password_confirm',
			                'label' => $this->lang->line('password_confirm'),
			                'rules' => 'trim|required'
			        ),
			        array(
			                'field' => 'email',
			                'label' => $this->lang->line('email'),
			                'rules' => 'trim|required|is_unique[users.email]|valid_email'
			        ),
			        array(
			                'field' => 'user_group',
			                'label' => $this->lang->line('user_group'),
			                'rules' => 'required|trim'
			        ),
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {    
	        	//Insert customers data   
				$user_data = array(
				       'username' => $this->input->post('username'),
				       'password' => $this->input->post('password'),
				       'email' => $this->input->post('email'),
				       'user_group' => $this->input->post('user_group'),
				       'active' => $this->input->post('user_active')

				);

				$this->db->insert('users', $user_data);
				$user_id = $this->db->insert_id();

				// Link user to specific Hosting admin
				if($this->input->post('hosting_id')){
					//Insert customers hosting data
					$hosting_data = array(
					       'user_id' => $user_id,
					       'hosting_id' => $this->input->post('hosting_id')
					);
					$this->db->insert('users_hosting', $hosting_data);
				}
				

				redirect('/users/', 'location');
				
	        }
	    }


	    $data['title'] = $this->lang->line('add_user');
		$this->data['content'] = $this->load->view('back/parts/add_user',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}


	public function edit_user($id)
	{
		
		if(!user_rules('admin')) {
        	redirect('/','location');
        }

		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
			        array(
			                'field' => 'password',
			                'label' => $this->lang->line('password'),
			                'rules' => 'trim|matches[password_confirm]|sha1'
			        ),
			        array(
			                'field' => 'password_confirm',
			                'label' => $this->lang->line('password_confirm'),
			                'rules' => 'trim'
			        ),
			        array(
			                'field' => 'email',
			                'label' => $this->lang->line('email'),
			                'rules' => 'trim|valid_email'
			        ),
			        array(
			                'field' => 'user_group',
			                'label' => $this->lang->line('user_group'),
			                'rules' => 'trim'
			        ),
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {    
	        	//Insert customers data   
				$user_data = array(
				       'username' => $this->input->post('username'),
				       'email' => $this->input->post('email'),
				       'user_group' => $this->input->post('user_group'),
				       'active' => $this->input->post('user_active')

				);
				if($this->input->post('password')!=''){
					$user_data['password'] = $this->input->post('password');
				}

				$this->db->where('id', $id);
				$this->db->update('users', $user_data);

				$hosting_id = $this->user_m->get_data('hosting_id', 'users_hosting',array('user_id'=>$id));
				
				// Link user to specific Hosting account	
				if($hosting_id){
					//Update customers hosting data
					$hosting_data = array(
					       'hosting_id' => $this->input->post('hosting_id')
					);
					$this->db->where('user_id', $id);
					$this->db->update('users_hosting', $hosting_data);
				}else {
					//Insert customers hosting data
					$hosting_data = array(
					       'user_id' => $id,
					       'hosting_id' => $this->input->post('hosting_id')
					);
					$this->db->insert('users_hosting', $hosting_data);
				}
				

				redirect('/users/', 'location');
				
	        }
	    }

		$data['title'] = $this->lang->line('edit_customer');

		$select = array('id','username','email','email','user_group','active');
		$where = array('id'=>$id);
	    $data['user'] = $this->user_m->get_data($select, 'users', $where);

	    $select = array('hosting_id as id');
		$where = array('user_id'=>$id);
	    $data['hosting'] = $this->user_m->get_data($select, 'users_hosting', $where);

	    if($data['hosting'] == NULL){
	    	$hosting = new stdClass;
	    	$hosting->id = '';
	    	$data['hosting']= $hosting;
	    }

	    if($data['user'] == NULL){
	    	redirect('/users/', 'location', 302);
	    }

		$this->data['content'] = $this->load->view('back/parts/edit_user',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}


	public function delete_user($id) {

		if(!user_rules('admin')) {
        	redirect('/','location');
        }

		$this->db->where(array('user_id'=>$id));
		$this->db->delete('users_hosting');

		$this->db->where(array('id'=>$id));
		$this->db->delete('users');

		redirect('/users/', 'location');
	}

	public function user_login()
	{
		if(user_rules('admin')) {
        	redirect('/admin','location');
        }elseif(user_rules('hosting')){
        	redirect('/','location');
        }
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
					array(
			                'field' => 'username',
			                'label' => $this->lang->line('username'),
			                'rules' => 'trim|required'
			        ),
			        array(
			                'field' => 'password',
			                'label' => $this->lang->line('password'),
			                'rules' => 'trim|required|sha1'
			        ),
				);
		 	$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {  
				$user_data = array(
				       'username' => $this->input->post('username'),
				       'password' => $this->input->post('password'),
				       'active' => 1
				);
			
				$user = $this->user_m->loged_in($user_data);
				
				if($user){
						$user_data = array(
							'user_id' => $user->id,
							'username' => $user->username,
							'group' => $user->user_group,
							'loged_in' => TRUE
						);
					$this->session->set_userdata($user_data);
					
					if(user_rules('admin')) {
			        	redirect('/admin','location');
			        }elseif(user_rules('hosting')){
			        	redirect('/','location');
			        }

				}else {

					$data['errors'] = $this->lang->line('wrong_login');
				}
			}
		}
		$data['title'] = $this->lang->line('user_login');
		

		$this->data['content'] = $this->load->view('back/parts/user_login',$data, TRUE);
		$this->load->view('back/login',$this->data);
	}

	public function user_logout()
	{
		if(!user_rules('hosting,admin')) {
        	redirect('/','location');
        }
		$this->session->sess_destroy();
		$this->session->set_userdata(array());
		redirect('/','location');

	}

}