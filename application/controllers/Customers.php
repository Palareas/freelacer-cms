<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {


	function __construct() {
        parent::__construct();
        $this->load->model('customers_m');
        if(!user_rules('admin')) {
        	redirect('/','location');
        }
        
    }
	public function index()
	{
		$data['title'] = $this->lang->line('customers_list');

		$select = array('id', 'owner_name', 'owner_surname','company_name');
		$data['customers'] = $this->customers_m->select_from($select,'customers');

		$this->data['content'] = $this->load->view('back/parts/customers_admin',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}
	public function add_customer()
	{
		
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
			        array(
			                'field' => 'name',
			                'label' => $this->lang->line('first_name'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'surname',
			                'label' => $this->lang->line('last_name'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'email',
			                'label' => $this->lang->line('email'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'username',
			                'label' => $this->lang->line('username'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'password',
			                'label' => $this->lang->line('password'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'domain',
			                'label' => $this->lang->line('domain'),
			                'rules' => 'required|trim|is_unique[domains.name]'
			        ),
			        array(
			                'field' => 'database',
			                'label' => $this->lang->line('database'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'db_username',
			                'label' => $this->lang->line('db_username'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'db_password',
			                'label' => $this->lang->line('db_password'),
			                'rules' => 'required|trim'
			        )
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {    
	        	//Insert customers data   
				$customer_data = array(
				       'owner_name' => $this->input->post('name'),
				       'owner_surname' => $this->input->post('surname'),
				       'email' => $this->input->post('email'),
				       'company_name' => $this->input->post('company_name'),
				       'company_address' => $this->input->post('company_address'),
				       'company_phone' => $this->input->post('company_phone'),
				       'company_desc' => $this->input->post('company_desc')

				);
				$this->db->insert('customers', $customer_data);
				$customer_id = $this->db->insert_id();

				// calculate time
				$time = new DateTime('NOW');
				$expire = clone $time;    
				$expire->modify('+1 year');

				//Insert customers hosting data
				$hosting_data = array(
				       'username' => $this->input->post('username'),
				       'password' => $this->input->post('password'),
				       'customer_id' => $customer_id,
				       'start' => $time->format('Y-m-d'),
				       'end' => $expire->format('Y-m-d')

				);
				$this->db->insert('hosting', $hosting_data);
				$hosting_id = $this->db->insert_id();

				//Insert customers domain data
				$domain_data = array(
				       'name' => $this->input->post('domain'),
				       'hosting_id' => $hosting_id,
				       'start' => $time->format('Y-m-d'),
				       'end' => $expire->format('Y-m-d'),
				       'main_domain' => 1
				);
				$this->db->insert('domains', $domain_data);


				$db_data = array(
						'name' => $this->input->post('database'),
				    	'username' => $this->input->post('db_username'),
				    	'password' => $this->input->post('db_password')
				);

				$this->db->insert('data', $db_data);
				$db_id = $this->db->insert_id();
				
				// Link DB to specific Hosting admin
				$db_hosting = array(
				       'data_id' => $db_id,
				       'hosting_id' => $hosting_id
				);
				$this->db->insert('data_hosting', $db_hosting);

				redirect('/customers/', 'location');
				
	        }
	    }


	    $data['title'] = $this->lang->line('add_customer');
		$this->data['content'] = $this->load->view('back/parts/add_customer',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function edit_customer($id)
	{
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
			        array(
			                'field' => 'name',
			                'label' => $this->lang->line('first_name'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'surname',
			                'label' => $this->lang->line('last_name'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'email',
			                'label' => $this->lang->line('email'),
			                'rules' => 'required|trim'
			        ),
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {        
				$customer_data = array(
				       'owner_name' => $this->input->post('name'),
				       'owner_surname' => $this->input->post('surname'),
				       'email' => $this->input->post('email'),
				       'company_name' => $this->input->post('company_name'),
				       'company_address' => $this->input->post('company_address'),
				       'company_phone' => $this->input->post('company_phone'),
				       'company_desc' => $this->input->post('company_desc')

				);
				$this->db->where('id', $id);
				$this->db->update('customers', $customer_data);

				redirect('/customers/', 'location');
				
	        }
	    }

		$data['title'] = $this->lang->line('edit_customer');

		$select = array('id','owner_name','owner_surname','email','company_name','company_address','company_phone','company_desc');
		$where = array('id'=>$id);
	    $data['customer'] = $this->customers_m->get_data($select, 'customers', $where);
	    
	    if($data['customer'] == NULL){
	    	redirect('/customers/', 'location', 302);
	    }

		$this->data['content'] = $this->load->view('back/parts/edit_customer',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}


}
