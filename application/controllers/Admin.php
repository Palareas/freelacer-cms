<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {


	function __construct() {
        parent::__construct();
        $this->load->model('customers_m');
        $this->load->model('hosting_m');
        $this->load->model('domains_m');
        $this->load->model('admin_m');
        if(!user_rules('admin')) {
        	redirect('/','location');
        }
    }
	public function index()
	{
		$data['title'] = $this->lang->line('admin_dashboard');
		
		$data['ftp_accounts'] = count($this->hosting_m->select_from('id','hosting'));

		$data['db_accounts'] = count($this->hosting_m->select_from('id','data'));

		$select = array('id', 'name', 'end');
		$data['domains'] = $this->hosting_m->select_from($select,'domains','end','ASC',5,0);

		$select = array('id', 'username', 'end');
		$data['hosting'] = $this->hosting_m->select_from($select,'hosting','end','ASC',5,0);
		
		$this->data['content'] = $this->load->view('back/parts/admin_home',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function bash()
	{
		$data['title'] = $this->lang->line('admin_home');

// 		<iframe  class="embed-responsive-item" src="http://instantdomainsearch.com/widget/liquid/" id="domain_search" name="domain_search" style="width:100%;height: 150px; overflow: hidden !important;" frameborder="0"
// scrolling="no">
// 				</iframe>

		//$data['test'] = shell_exec("ls 2>&1 &");
		//$data['output'] = shell_exec("../test.sh");
		
		//$d = disk_total_space("/var/www/medenikutak");
		// $disk_size = disk_total_space("/");
		// $disk_free_space = disk_free_space("/");
		// $disk_in_use = $disk_size - $disk_free_space;

		// function addUnits($bytes) {

		// 		$units = array( 'B', 'KB', 'MB', 'GB', 'TB' );

		// 		for($i = 0; $bytes >= 1024 && $i < count($units) - 1; $i++ ) {

		// 		$bytes /= 1024;

		// 		}
		// 		return round($bytes, 1).' '.$units[$i];
		// }

		// echo addUnits($disk_size). "<br/>";
		// echo addUnits($disk_free_space). "<br/>";
		// echo addUnits($disk_in_use). "<br/>";
		// echo getcwd();
		
		// function folderSize($dir){
		// 	$count_size = 0;
		// 	$count = 0;
		// 	$dir_array = scandir($dir);
		// 	  foreach($dir_array as $key=>$filename){
		// 	    if($filename!=".." && $filename!="."){
		// 	       if(is_dir($dir."/".$filename)){
		// 	          $new_foldersize = foldersize($dir."/".$filename);
		// 	          $count_size = $count_size+ $new_foldersize;
		// 	        }else if(is_file($dir."/".$filename)){
		// 	          $count_size = $count_size + filesize($dir."/".$filename);
		// 	          $count++;
		// 	        }
		// 	   }
		// 	 }
		// 	return $count_size;
		// 	}
		 //$d = number_format($d / 1073741824, 2) . ' GB';
		// var_dump(addUnits(folderSize('/Applications/MAMP/htdocs/medenikutak/public_html')));
		//$data['test'] = get_disk_size() .' - '. get_disk_free() .' - '. get_disk_in_use() . ' / ' .  get_disk_percent();

		var_dump(get_disk_info());

		$this->data['content'] = $this->load->view('back/parts/blank',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	
	public function sql()
	{
		$data['title'] = $this->lang->line('admin_home');

		if($_POST) {
				$database_data = array(
				       'database' => $this->input->post('database'),
				       'username' => $this->input->post('username'),
				       'password' => $this->input->post('password')

				);
				$this->admin_m->create_db($database_data);
		}
		//$data['output'] = shell_exec("../test.sh 2>&1 &");
		//$data['output'] = shell_exec("../test.sh");
		
		$this->data['content'] = $this->load->view('back/parts/create_db',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function tasks()
	{
		$data['title'] = $this->lang->line('admin_home');

		//https://api.trello.com/1/members/me/boards/89ByQ0Xd?key=c83920bb912f2cf78cb318f98183f07f&token=282c2a21193ee721c5e586be146e77c50fce21b430b1c764d195ade3d988c2b6


		//$data['output'] = shell_exec("../test.sh 2>&1 &");
		//$data['output'] = shell_exec("../test.sh");
		$this->data['content'] = '

		<table class="table">
		    <thead>
				<tr>
					<th>Naziv</th>
					<th>opis</th>
					<th>link</th>
				</tr>
      		</thead>
			<tbody id="trello">
			</tbody>
		  </table>';
		$this->load->view('back/index',$this->data);
	}

	
}
