<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends MY_Controller {


	function __construct() {
        parent::__construct();
        $this->load->model('customers_m');
        $this->load->model('hosting_m');
        $this->load->model('domains_m');
        $this->load->model('admin_m');
        if(!user_rules('admin')) {
        	redirect('/','location');
        }
    }
	public function index()
	{
		$data['title'] = $this->lang->line('tasks_admin');
		
		$select = array('id', 'username', 'trello');
		$data['users'] = $this->hosting_m->get_all($select,'hosting', array('trello !=' => ''));
		
		$this->data['content'] = $this->load->view('back/parts/tasks_admin',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function get_tasks()
	{
		$data['title'] = $this->lang->line('tasks_list');

		//https://api.trello.com/1/members/me/boards/89ByQ0Xd?key=c83920bb912f2cf78cb318f98183f07f&token=282c2a21193ee721c5e586be146e77c50fce21b430b1c764d195ade3d988c2b6

		$this->data['content'] = $this->load->view('back/parts/tasks_list',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	
}
