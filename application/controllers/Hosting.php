<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hosting extends MY_Controller {


	function __construct() {
        parent::__construct();
        $this->load->model('hosting_m');
        if(!user_rules('admin')) {
        	redirect('/','location');
        }
        
    }
	public function index()
	{
		$data['title'] = $this->lang->line('hosting_list');

		$data['hosting'] = $this->hosting_m->get_hosting_list('end','asc');

		$this->data['content'] = $this->load->view('back/parts/hosting_admin',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}
	public function add_hosting()
	{
		
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
					array(
			                'field' => 'domain',
			                'label' => $this->lang->line('domain'),
			                'rules' => 'required|trim|is_unique[domains.name]'
			        ),
			        array(
			                'field' => 'username',
			                'label' => $this->lang->line('username'),
			                'rules' => 'required|trim|is_unique[hosting.username]'
			        ),
			        array(
			                'field' => 'password',
			                'label' => $this->lang->line('password'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'owner',
			                'label' => $this->lang->line('owner'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'start',
			                'label' => $this->lang->line('start'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'end',
			                'label' => $this->lang->line('end'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'package',
			                'label' => $this->lang->line('hp_name'),
			                'rules' => 'required|trim'
			        )
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {        
				$hosting_data = array(
				       'username' => $this->input->post('username'),
				       'password' => $this->input->post('password'),
				       'customer_id' => $this->input->post('owner'),
				       'start' => $this->input->post('start'),
				       'end' => $this->input->post('end'),
				       'package' => $this->input->post('package'),
				       'trello' => $this->input->post('trello')

				);

				$this->db->insert('hosting', $hosting_data);
				$hosting_id = $this->db->insert_id();

				$domain_data = array(
				       'name' => $this->input->post('domain'),
				       'hosting_id' => $hosting_id,
				       'start' => $this->input->post('start'),
				       'end' => $this->input->post('end'),
				       'main_domain' => 1

				);

				$this->db->insert('domains', $domain_data);

				redirect('/hosting/', 'location');
				
	        }
	    }


	    $data['title'] = $this->lang->line('add_hosting');


		$this->data['content'] = $this->load->view('back/parts/add_hosting',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function edit_hosting($id)
	{
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
			        array(
			                'field' => 'password',
			                'label' => $this->lang->line('password'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'owner',
			                'label' => $this->lang->line('owner'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'start',
			                'label' => $this->lang->line('start'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'end',
			                'label' => $this->lang->line('end'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'package',
			                'label' => $this->lang->line('hp_name'),
			                'rules' => 'required|trim'
			        )
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {        
				$hosting_data = array(
				       'password' => $this->input->post('password'),
				       'customer_id' => $this->input->post('owner'),
				       'start' => $this->input->post('start'),
				       'end' => $this->input->post('end'),
				       'package' => $this->input->post('package'),
				       'trello' => $this->input->post('trello')

				);

				$this->db->where('id', $id);
				$this->db->update('hosting', $hosting_data);

				$domain_data = array(
				       'start' => $this->input->post('start'),
				       'end' => $this->input->post('end')

				);
				$this->db->where('hosting_id', $id);
				$this->db->where('main_domain', 1);
				$this->db->update('domains', $domain_data);

				redirect('/hosting/', 'location');
				
	        }
	    }

		$data['title'] = $this->lang->line('edit_hosting');

		$select = array('id','username','password','customer_id','start','end','package','trello');
		$where = array('id'=>$id);
	    $data['hosting'] = $this->hosting_m->get_data($select, 'hosting', $where);
	    
	    if($data['hosting'] == NULL){
	    	redirect('/hosting/', 'location', 302);
	    }

		$this->data['content'] = $this->load->view('back/parts/edit_hosting',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function packages()
	{
		$data['title'] = $this->lang->line('packages_list');

		$select = array('id', 'name', 'price','db_numb','domains');
		$data['packages'] = $this->hosting_m->select_from($select,'hosting_packages');

		$this->data['content'] = $this->load->view('back/parts/hosting_package_admin',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function add_package()
	{
		
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
			        array(
			                'field' => 'name',
			                'label' => $this->lang->line('hp_name'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'price',
			                'label' => $this->lang->line('price'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'db_numb',
			                'label' => $this->lang->line('db_numb'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'domains',
			                'label' => $this->lang->line('domains_numb'),
			                'rules' => 'required|trim'
			        )
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {        
				$hp_data = array(
				       'name' => $this->input->post('name'),
				       'price' => $this->input->post('price'),
				       'db_numb' => $this->input->post('db_numb'),
				       'domains' => $this->input->post('domains')

				);

				$this->db->insert('hosting_packages', $hp_data);

				redirect('/hosting/packages/', 'location');
				
	        }
	    }


	    $data['title'] = $this->lang->line('add_hosting_package');
		$this->data['content'] = $this->load->view('back/parts/add_hosting_package',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function edit_package($id)
	{
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
			        array(
			                'field' => 'name',
			                'label' => $this->lang->line('hp_name'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'price',
			                'label' => $this->lang->line('price'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'db_numb',
			                'label' => $this->lang->line('db_numb'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'domains',
			                'label' => $this->lang->line('domains_numb'),
			                'rules' => 'required|trim'
			        )
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {        
				$hp_data = array(
				       'name' => $this->input->post('name'),
				       'price' => $this->input->post('price'),
				       'db_numb' => $this->input->post('db_numb'),
				       'domains' => $this->input->post('domains')

				);
				$this->db->where('id', $id);
				$this->db->update('hosting_packages', $hp_data);

				redirect('/hosting/packages/', 'location');
				
	        }
	    }

		$data['title'] = $this->lang->line('edit_hosting_package');

		$select = array('id', 'name', 'price','db_numb','domains');
		$where = array('id'=>$id);
	    $data['package'] = $this->hosting_m->get_data($select, 'hosting_packages', $where);
	    
	    if($data['package'] == NULL){
	    	redirect('/hosting/packages/', 'location', 302);
	    }

		$this->data['content'] = $this->load->view('back/parts/edit_hosting_package',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

}
