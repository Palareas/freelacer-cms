<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo 'Virtual HOST - the best hosting solutions';?></title>
	<!-- Bootstrap core CSS -->
    <link href="<?php echo site_url('css/bootstrap.css');?>" rel="stylesheet">
    <!-- Font-Awesome -->
    <link href="<?php echo site_url('css/font-awesome.min.css');?>" rel="stylesheet">

    <link href="<?php echo site_url('css/site.css');?>" rel="stylesheet">

    <style>
		.modal-backdrop {
		  z-index: -1;
		}
    </style>

</head>
<body>