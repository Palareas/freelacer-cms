<?php $this->load->view('back/header.php');?>
	<nav class="navbar navbar-default ">
	  <div class="container">

	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#admin-menu">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo get_menu_link('admin');?>"><?php echo $this->lang->line('page_name');?></a>
	    </div>

	    <div class="collapse navbar-collapse" id="admin-menu">
	      <ul class="nav navbar-nav"> 
	        <li class="<?php echo menu_active('admin');?>"><a href="<?php echo get_menu_link('admin');?>"><?php echo $this->lang->line('admin_home'); ?></a></li>
	        <li class="<?php echo menu_active('customers');?>"><a href="<?php echo get_menu_link('customers');?>"><?php echo $this->lang->line('admin_customers'); ?></a></li>
	        <li class="<?php echo menu_active('hosting');?>"><a href="<?php echo get_menu_link('hosting');?>"><?php echo $this->lang->line('admin_hosting'); ?></a></li>
	        <li class="<?php echo menu_active('domains');?>"><a href="<?php echo get_menu_link('domains');?>"><?php echo $this->lang->line('admin_domains'); ?></a></li>
	        <li class="<?php echo menu_active('data');?>"><a href="<?php echo get_menu_link('data');?>"><?php echo $this->lang->line('admin_database'); ?></a></li>
			<li class="<?php echo menu_active('tasks');?>"><a href="<?php echo get_menu_link('tasks');?>"><?php echo $this->lang->line('admin_tasks'); ?></a></li>
	        <li class="<?php echo menu_active('users');?>"><a href="<?php echo get_menu_link('users');?>"><?php echo $this->lang->line('admin_users'); ?></a></li>
			<?php if($this->session->userdata('username')):?>
			<li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $this->session->userdata('username') ?> <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="<?php echo base_url('users/user_logout');?>"> <?php echo $this->lang->line('logout'); ?> </a></li>
	          </ul>
	        </li>
	    	<?php endif;?>
	      </ul>
	    </div>
	  </div>
	</nav>
	<div class="container">
		<?php echo $content;?>
	</div>
<?php $this->load->view('back/modals-popup.php');?>
<?php $this->load->view('back/footer.php');?>
