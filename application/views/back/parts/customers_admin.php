<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
	    <table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th><?php echo $this->lang->line('owner');?></th>
					<th><?php echo $this->lang->line('company_name');?></th>
					<th><?php echo $this->lang->line('actions');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($customers as $customer):?>
				<tr>
					<th><?php echo $customer->id;?></th>
					<td><?php echo $customer->owner_name .' '.$customer->owner_surname;?></td>
					<td><?php echo $customer->company_name;?></td>
					<td><a class="btn btn-default" href="<?php echo base_url("customers/edit_customer/$customer->id");?>" role="button"><?php echo $this->lang->line('edit');?></a></td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<div class="panel panel-default">
			<div class="panel-body">
				<a href="<?php echo base_url('/customers/add_customer');?>" class="btn btn-success " role="button">
					<?php echo $this->lang->line('add_customer');?>
				</a>
			</div>
		</div>
	</div>
</div>
