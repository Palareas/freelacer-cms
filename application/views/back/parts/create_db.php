<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
		<div class="well">
			<?php if(!empty($main_error)):?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $main_error;?>
				</div>
				
			<?php endif;?>
            <form class="form-horizontal" method="post" action="<?php echo base_url('admin/bash'); ?>">
				<fieldset>
					<div class="form-group <?php echo form_error('database')? 'has-error': '';?>">
						<label for="database" class="col-lg-2 control-label"><?php echo $this->lang->line('domain');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="database" name="database" value="<?php echo set_value('database'); ?>" placeholder="<?php echo form_error('database')? form_error('database'): $this->lang->line('domain');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('username')? 'has-error': '';?>">
						<label for="username" class="col-lg-2 control-label"><?php echo $this->lang->line('username');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="username" name="username" value="<?php echo set_value('username'); ?>" placeholder="<?php echo form_error('username')? form_error('username'): $this->lang->line('username');?>">
						</div>
					</div>
					<div class="form-group">
					  	<label for="password <?php echo form_error('password')? 'has-error': '';?>" class="col-lg-2 control-label"><?php echo $this->lang->line('password');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="password" name="password" value="<?php echo set_value('password')? set_value('password'): get_random_password(); ?>">
						</div>
					</div>
					<div class="form-group">
					  <div class="col-lg-10 col-lg-offset-2">
					    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#cancel-confirm"><?php echo $this->lang->line('cancel');?></button>
					    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save');?></button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
