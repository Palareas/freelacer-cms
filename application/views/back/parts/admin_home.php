<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="panel panel-default">
		  <div class="panel-heading"><?php echo $this->lang->line('server_disk_info');?></div>
		  <div class="panel-body">
				<p><?php echo $this->lang->line('space_avelable');?> <strong><?php echo get_disk_info('disk_space');?></strong></p>
				<p><?php echo $this->lang->line('space_in_use');?> <strong><?php echo get_disk_info('in_use');?></strong></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo get_disk_info('percent');?>">
				    <?php echo get_disk_info('percent');?>
				  </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default">
		  <div class="panel-heading"><?php echo $this->lang->line('ftp_users');?></div>
		  <div class="panel-body">
				<p><?php echo $this->lang->line('recomended');?> <strong>25</strong></p>
				<p><?php echo $this->lang->line('created');?> <strong><?php echo $ftp_accounts;?></strong></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo get_disk_percent($ftp_accounts, 25);?>">
				    <?php echo get_disk_percent($ftp_accounts, 25);?>
				  </div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default">
		  <div class="panel-heading"><?php echo $this->lang->line('db_info');?></div>
		  <div class="panel-body">
				<p><?php echo $this->lang->line('recomended');?> <strong>25</strong></p>
				<p><?php echo $this->lang->line('created');?> <strong><?php echo $db_accounts;?></strong></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo get_disk_percent($db_accounts, 25);?>">
				    <?php echo get_disk_percent($db_accounts, 25);?>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
		  <div class="panel-heading"><?php echo $this->lang->line('domain_list');?></div>
		  <table class="table">
		    <thead>
				<tr>
					<th><?php echo $this->lang->line('domain');?></th>
					<th><?php echo $this->lang->line('end');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($domains as $domain):?>
				<tr class="<?php echo expire($domain->end);?>">
					<td><?php echo $domain->name;?></td>
					<td><?php echo date("d.m.Y", strtotime($domain->end));?></td>
				</tr>
				<?php endforeach;?>
			</tbody>
		  </table>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
		  <div class="panel-heading"><?php echo $this->lang->line('hosting_list');?></div>
		  <table class="table">
		    <thead>
				<tr>
					<th><?php echo $this->lang->line('username');?></th>
					<th><?php echo $this->lang->line('end');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($hosting as $hosting):?>
				<tr class="<?php echo expire($hosting->end);?>">
					<td><?php echo $hosting->username;?></td>
					<td><?php echo date("d.m.Y", strtotime($hosting->end));?></td>
				</tr>
				<?php endforeach;?>
			</tbody>
		  </table>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<a href="<?php echo base_url('/customers/add_customer');?>" class="btn btn-success  btn-lg btn-block" role="button">
							<?php echo $this->lang->line('add_customer');?>
						</a>
					</div>
					<div class="col-md-4">
						<a href="<?php echo base_url('/hosting/add_hosting');?>" class="btn btn-success  btn-lg btn-block" role="button">
							<?php echo $this->lang->line('add_hosting');?>
						</a>
					</div>
					<div class="col-md-4">
						<a href="<?php echo base_url('/domains/add_domain');?>" class="btn btn-success  btn-lg btn-block" role="button">
							<?php echo $this->lang->line('add_domain');?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
