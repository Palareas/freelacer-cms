<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
	    <table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th><?php echo $this->lang->line('db_name');?></th>
					<th><?php echo $this->lang->line('username');?></th>
					<th><?php echo $this->lang->line('actions');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($databases as $db):?>
				<tr>
					<th><?php echo $db->id;?></th>
					<td><?php echo $db->name;?></td>
					<td><?php echo $db->username;?></td>
					<td>
						<a class="btn btn-default" href="<?php echo base_url("data/edit_data/$db->id");?>" role="button">
							<?php echo $this->lang->line('edit');?>
						</a>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<div class="panel panel-default">
			<div class="panel-body">
				<a href="<?php echo base_url('/data/add_data');?>" class="btn btn-success" role="button">
					<?php echo $this->lang->line('add_db');?>
				</a>
			</div>
		</div>
	</div>
</div>


