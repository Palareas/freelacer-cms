<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
		<div class="well">
			<?php if(!empty($main_error)):?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $main_error;?>
				</div>
				
			<?php endif;?>
            <form class="form-horizontal" method="post" action="<?php echo base_url("users/edit_user/$user->id"); ?>">
				<fieldset>
					<legend><?php echo $this->lang->line('user_data');?></legend>
					<div class="form-group <?php echo form_error('username')? 'has-error': '';?>">
						<label for="username" class="col-lg-2 control-label"><?php echo $this->lang->line('username');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="username" name="username" value="<?php echo $user->username; ?>" placeholder="<?php echo form_error('username')? form_error('username'): $this->lang->line('username');?>">
						</div>
					</div>
					<div class="form-group">
					  	<label for="password <?php echo form_error('password')? 'has-error': '';?>" class="col-lg-2 control-label"><?php echo $this->lang->line('password');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="password" name="password" value="" placeholder="<?php echo $this->lang->line('password_new');?>">
						</div>
					</div>
					<div class="form-group">
					  	<label for="password_confirm <?php echo form_error('password_confirm')? 'has-error': '';?>" class="col-lg-2 control-label"><?php echo $this->lang->line('password_confirm');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="password_confirm" name="password_confirm" value="" placeholder="<?php echo $this->lang->line('password_confirm');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('email')? 'has-error': '';?>">
					  	<label for="email" class="col-lg-2 control-label"><?php echo $this->lang->line('email');?></label>
					  	<div class="col-lg-10">
				    		<input type="text" class="form-control" id="email" name="email" value="<?php echo $user->email; ?>" placeholder="<?php echo form_error('email') ? form_error('email') : $this->lang->line('email');?>">
					  	</div>
					</div>
					<legend><?php echo $this->lang->line('user_settings');?></legend>

					<div class="form-group <?php echo form_error('hosting_id')? 'has-error': '';?>">
					  	<label for="hosting_id" class="col-lg-2 control-label"><?php echo $this->lang->line('ftp_account');?></label>
					  	<div class="col-lg-10">
					  		<select class="form-control" name="hosting_id" id="hosting_id" value="<?php echo $hosting->id; ?>">
					  			<?php foreach (get_hosting() as $id => $username):?>
					  				<option value="<?php echo $id;?>" <?php echo $id == $hosting->id ? 'selected="selected"':'';?>><?php echo $username;?></option>
				                <?php endforeach;?>
					  		</select>
					  	</div>
					</div>
					
					<div class="form-group <?php echo form_error('user_group')? 'has-error': '';?>">
					  	<label for="user_group" class="col-lg-2 control-label"><?php echo $this->lang->line('user_group');?></label>
					  	<div class="col-lg-10">
					  		<select class="form-control" name="user_group" id="user_group" value="<?php echo $user->user_group; ?>">
					  			<?php foreach (get_user_group() as $id => $group):?>
					  				<option value="<?php echo $id;?>" <?php echo $id == $user->user_group ? 'selected="selected"':'';?>><?php echo $group;?></option>
				                <?php endforeach;?>
					  		</select>
					  	</div>
					</div>
					<div class="form-group">
					  	<label for="user_active" class="col-lg-2 control-label"><?php echo $this->lang->line('user_active');?></label>
					  	<div class="col-lg-10">
					    	<input type="text" class="form-control" id="user_active" name="user_active" value="<?php echo $user->active; ?>" placeholder="<?php echo $this->lang->line('user_active');?>">
					  	</div>
					</div>
					<legend></legend>
					<div class="form-group">
					  <div class="col-lg-10 col-lg-offset-2">
					    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#cancel-confirm"><?php echo $this->lang->line('cancel');?></button>
					    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save');?></button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>