<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
		<div class="well">
			<?php if(!empty($main_error)):?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $main_error;?>
				</div>
				
			<?php endif;?>
            <form class="form-horizontal" method="post" action="<?php echo base_url("hosting/edit_package/$package->id"); ?>">
				<fieldset>
					<legend><?php echo $this->lang->line('package_desc');?></legend>
					
					<div class="form-group <?php echo form_error('name')? 'has-error': '';?>"> 
						<label for="name" class="col-lg-2 control-label"><?php echo $this->lang->line('hp_name');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="name" name="name" value="<?php echo $package->name; ?>" placeholder="<?php echo form_error('name')? form_error('name'): $this->lang->line('hp_name');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('price')? 'has-error': '';?>">
					  	<label for="price" class="col-lg-2 control-label"><?php echo $this->lang->line('price');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="price" name="price" value="<?php echo $package->price; ?>" placeholder="<?php echo form_error('price')? form_error('price'): $this->lang->line('price');?>">
						</div>
					</div>
					<legend><?php echo $this->lang->line('package_limits');?></legend>
					<div class="form-group <?php echo form_error('db_numb')? 'has-error': '';?>">
					  	<label for="db_numb" class="col-lg-2 control-label"><?php echo $this->lang->line('db_numb');?></label>
					  	<div class="col-lg-10">
				    		<input type="text" class="form-control" id="db_numb" name="db_numb" value="<?php echo $package->db_numb; ?>" placeholder="<?php echo form_error('db_numb') ? form_error('db_numb') : $this->lang->line('db_numb');?>">
					  	</div>
					</div>
					<div class="form-group <?php echo form_error('domains')? 'has-error': '';?>">
						<label for="domains" class="col-lg-2 control-label"><?php echo $this->lang->line('domain_numb');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="domains" name="domains" value="<?php echo $package->domains; ?>" placeholder="<?php echo form_error('domains')? form_error('domains'): $this->lang->line('domain_numb');?>">
						</div>
					</div>
					<legend></legend>
					<div class="form-group">
					  <div class="col-lg-10 col-lg-offset-2">
					    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#cancel-confirm"><?php echo $this->lang->line('cancel');?></button>
					    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save');?></button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
