<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-md-12">
		  <table class="table table-striped table-hover">
		    <thead>
				<tr>
					<th><?php echo $this->lang->line('task_name');?></th>
					<th><?php echo $this->lang->line('task_desc');?></th>
					<th><?php echo $this->lang->line('task_flags');?></th>
					<th><?php echo $this->lang->line('task_status');?></th>
					<th><?php echo $this->lang->line('task_link');?></th>
				</tr>
      		</thead>
			<tbody id="trello">
			</tbody>
		  </table>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<?php if(isset($_GET['token'])): ?>
							<a href="<?php echo base_url('tasks/?token=').$_GET['token'] ;?>" class="btn btn-success " role="button" >
								<?php echo $this->lang->line('back'); ?>
							</a>
						<?php else :?>
							<a href="<?php echo base_url('tasks');?>" class="btn btn-success " role="button" ><?php echo $this->lang->line('back'); ?></a>
						<?php endif;?>
						<a href='<?php echo "https://trello.com/b/". $_GET['board']. "/";?> ' class="btn btn-success " role="button" target="_blank">
							<?php echo $this->lang->line('add_new_task');?>
						</a>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>