<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
		<div class="well">
			<?php if(!empty($main_error)):?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $main_error;?>
				</div>
				
			<?php endif;?>
            <form class="form-horizontal" method="post" action="<?php echo base_url("hosting/edit_hosting/$hosting->id"); ?>">
				<fieldset>
					<legend><?php echo $this->lang->line('customers_data');?></legend>
					<div class="form-group <?php echo form_error('owner')? 'has-error': '';?>">
					  	<label for="owner" class="col-lg-2 control-label"><?php echo $this->lang->line('owner');?></label>
					  	<div class="col-lg-10">
					  		<select class="form-control" name="owner" id="owner" value="<?php echo $hosting->customer_id; ?>">
					  			<?php foreach (get_owner() as $id => $owner):?>
					  				<option value="<?php echo $id;?>" <?php echo $id == $hosting->customer_id ? 'selected="selected"':'';?>><?php echo $owner;?></option>
				                <?php endforeach;?>
					  		</select>
					  	</div>
					</div>
					<legend><?php echo $this->lang->line('ftp_data');?></legend>
					<div class="form-group <?php echo form_error('username')? 'has-error': '';?>">
						<label for="username" class="col-lg-2 control-label"><?php echo $this->lang->line('username');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="username" name="username" value="<?php echo $hosting->username; ?>" disabled>
						</div>
					</div>
					<div class="form-group">
					  	<label for="password <?php echo form_error('password')? 'has-error': '';?>" class="col-lg-2 control-label"><?php echo $this->lang->line('password');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="password" name="password" value="<?php echo $hosting->password ? $hosting->password: get_random_password(); ?>">
						</div>
					</div>

					<legend><?php echo $this->lang->line('package_time');?></legend>
					<div class="form-group <?php echo form_error('start')? 'has-error': '';?>">
					  	<label for="start" class="col-lg-2 control-label"><?php echo $this->lang->line('start');?></label>
					  	<div class="col-lg-10">
					  		<?php 
					  		$time = new DateTime($hosting->start);
					  		?>
					    	<input type="text" class="form-control" id="start" name="start" value="<?php echo $time->format( 'Y-m-d' );; ?>" placeholder="<?php echo $this->lang->line('start');?>">
					  	</div>
					</div>
					<div class="form-group <?php echo form_error('end')? 'has-error': '';?>">
					  	<label for="end" class="col-lg-2 control-label"><?php echo $this->lang->line('end');?></label>
					  	<?php 

					  		$expire = clone $time;    
  							$expire->modify('+1 year');
					  	?>
					  	<div class="col-lg-10">
					    	<input type="text" class="form-control" id="end" name="end" value="<?php echo $expire->format('Y-m-d'); ?>" placeholder="<?php echo $this->lang->line('end');?>">
					  	</div>
					</div>
					<legend><?php echo $this->lang->line('hosting_package');?></legend>
					<div class="form-group <?php echo form_error('package')? 'has-error': '';?>">
					  	<label for="package" class="col-lg-2 control-label"><?php echo $this->lang->line('hp_name');?></label>
					  	<div class="col-lg-10">
					  		<select class="form-control" name="package" id="package" value="<?php echo set_value('package'); ?>">
					  			<?php foreach (get_package() as $id => $category):?>
					  				<option value="<?php echo $id;?>"  <?php echo $id == $hosting->package ? 'selected="selected"':'';?>><?php echo $category;?></option>
				                <?php endforeach;?>
					  		</select>
					  	</div>
					</div>

					<legend><?php echo $this->lang->line('trello_board');?></legend>
					<div class="form-group">
					  	<label for="trello <?php echo form_error('trello')? 'has-error': '';?>" class="col-lg-2 control-label"><?php echo $this->lang->line('trello');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="trello" name="trello" <?php echo $hosting->trello ? "value='" . $hosting->trello ."'": "placeholder='" . $this->lang->line('trello') ."'"; ?> >
						</div>
					</div>

					<legend></legend>
					<div class="form-group">
					  <div class="col-lg-10 col-lg-offset-2">
					    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#cancel-confirm"><?php echo $this->lang->line('cancel');?></button>
					    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save');?></button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
