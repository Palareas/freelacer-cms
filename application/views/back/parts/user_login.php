<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<div class="page-header">
			<img src="<?php echo base_url('images/logo.png');?>" alt="">
			<!-- <h4><?php echo $title;?></h4> -->
		</div>
		<div class="well">
			<?php if(!empty($main_error)):?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $main_error;?>
				</div>
				
			<?php endif;?>
            <form class="form-horizontal" method="post" action="<?php echo base_url('users/user_login'); ?>">
				<fieldset>
					<div class="form-group <?php echo form_error('username')? 'has-error': '';?>">
						<label for="username" class="col-lg-4 control-label"><?php echo $this->lang->line('username');?></label>
						<div class="col-lg-8">
							<input type="text" class="form-control" id="username" name="username" value="<?php echo set_value('username'); ?>" placeholder="<?php echo form_error('username')? form_error('username'): $this->lang->line('username');?>">
						</div>
					</div>
					<div class="form-group">
					  	<label for="password <?php echo form_error('password')? 'has-error': '';?>" class="col-lg-4 control-label"><?php echo $this->lang->line('password');?></label>
						<div class="col-lg-8">
							<input type="password" class="form-control" id="password" name="password" placeholder="<?php echo $this->lang->line('password');?>">
						</div>
					</div>
					<div class="form-group">
					  <div class="col-lg-12">
					    <button type="submit" class="btn btn-success btn-block"><?php echo $this->lang->line('login');?></button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>