<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
		<div class="well">
			<?php if(!empty($main_error)):?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $main_error;?>
				</div>
				
			<?php endif;?>
            <form class="form-horizontal" method="post" action="<?php echo base_url('domains/add_domain'); ?>">
				<fieldset>
					<legend><?php echo $this->lang->line('ftp_data');?></legend>
					<div class="form-group <?php echo form_error('hosting_id')? 'has-error': '';?>">
					  	<label for="hosting_id" class="col-lg-2 control-label"><?php echo $this->lang->line('username');?></label>
					  	<div class="col-lg-10">
					  		<select class="form-control" name="hosting_id" id="hosting_id" value="<?php echo set_value('hosting_id'); ?>">
					  			<?php foreach (get_hosting() as $id => $username):?>
					  				<option value="<?php echo $id;?>" <?php echo $id == set_value('hosting_id') ? 'selected="selected"':'';?>><?php echo $username;?></option>
				                <?php endforeach;?>
					  		</select>
					  	</div>
					</div>
					<legend><?php echo $this->lang->line('domain_data');?></legend>
					<div class="form-group <?php echo form_error('name')? 'has-error': '';?>">
						<label for="name" class="col-lg-2 control-label"><?php echo $this->lang->line('domain');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="<?php echo form_error('name')? form_error('name'): $this->lang->line('name');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('start')? 'has-error': '';?>">
					  	<label for="start" class="col-lg-2 control-label"><?php echo $this->lang->line('start');?></label>
					  	<div class="col-lg-10">
					  		<?php 
					  		$time = new DateTime('NOW');
					  		?>
					    	<input type="text" class="form-control" id="start" name="start" value="<?php echo set_value('start')? set_value('start'): $time->format( 'Y-m-d' );; ?>" placeholder="<?php echo $this->lang->line('start');?>">
					  	</div>
					</div>
					<div class="form-group <?php echo form_error('end')? 'has-error': '';?>">
					  	<label for="end" class="col-lg-2 control-label"><?php echo $this->lang->line('end');?></label>
					  	<?php 

					  		$expire = clone $time;    
  							$expire->modify('+1 year');
					  	?>
					  	<div class="col-lg-10">
					    	<input type="text" class="form-control" id="end" name="end" value="<?php echo set_value('end')? set_value('end'): $expire->format('Y-m-d'); ?>" placeholder="<?php echo $this->lang->line('end');?>">
					  	</div>
					</div>
					<div class="form-group <?php echo form_error('price')? 'has-error': '';?>">
						<label for="price" class="col-lg-2 control-label"><?php echo $this->lang->line('price');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="price" name="price" value="<?php echo set_value('price'); ?>" placeholder="<?php echo form_error('price')? form_error('price'): $this->lang->line('price');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('main_domain')? 'has-error': '';?>">
						<label for="main_domain" class="col-lg-2 control-label"><?php echo $this->lang->line('main_domain');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="main_domain" name="main_domain" value="<?php echo set_value('main_domain'); ?>" placeholder="<?php echo form_error('main_domain')? form_error('main_domain'): $this->lang->line('main_domain');?>">
						</div>
					</div>

					<legend></legend>
					<div class="form-group">
					  <div class="col-lg-10 col-lg-offset-2">
					    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#cancel-confirm"><?php echo $this->lang->line('cancel');?></button>
					    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save');?></button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
