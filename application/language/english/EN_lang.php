<?php 

$lang['page_name'] = "Admin page";

$lang['admin_home'] = "Dashboard";
$lang['admin_dashboard'] = "Admin dashboard";
$lang['admin_customers'] = "Customers";
$lang['admin_hosting'] = "Hosting";
$lang['admin_domains'] = "Domains";
$lang['admin_users'] = "Users";
$lang['admin_database'] = "Database";
$lang['admin_tasks'] = "Tasks";



$lang['actions'] = "Actions";
$lang['edit'] = "Edit";
$lang['cancel'] = "Cancel";
$lang['confirm'] = "Confirm";
$lang['save'] = "Save";
$lang['back'] = "Back";
$lang['delete'] = "Delete";
$lang['db_view'] = "DB list";

$lang['price'] = "Price";
$lang['start'] = "Registered";
$lang['end'] = "Expire";
$lang['package_time'] = "Package expiration time";


$lang['data_alert'] = "Data aren't saved!";
$lang['data_alert_body'] = "<strong>Warning!</strong> <br/> Do you really want to leave this form?";

$lang['main_error'] = "<strong>Warning!</strong> Please fill in all of the required fields";



/* Customer */
$lang['customers_list'] = "Customers list";
$lang['add_customer'] = "Add new Customer";
$lang['edit_customer'] = "Edit Customer";
$lang['customers_data'] = "Customer data";
$lang['ftp_data'] = "FTP data";
$lang['ftp_account'] = "FTP account";



$lang['owner'] = "Owner name";
$lang['select_owner'] = "Select owner";
$lang['company_data'] = "Company data";
$lang['company_name'] = "Company name";
$lang['company_address'] = "Company address";
$lang['company_phone'] = "Company phone";
$lang['company_desc'] = "Company description";





$lang['first_name'] = 'First Name';
$lang['last_name'] = 'Last Name';
$lang['email'] = 'Email';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['password_confirm'] = 'Confirm password';
$lang['password_new'] = 'New password';




$lang['hp_name'] = 'Package name';
$lang['add_hosting_package'] = 'Add hosting package';
$lang['edit_hosting_package'] = 'Edit hosting package';
$lang['db_numb'] = 'DB limit';
$lang['db_created'] = 'DB created';

$lang['domain_numb'] = 'Domains limit';
$lang['package_desc'] = 'Package description';
$lang['package_limits'] = 'Package limits';

$lang['packages_list'] = 'Packages list';


$lang['add_hosting'] = 'Create new hosting';
$lang['edit_hosting'] = 'Edit hosting';

$lang['hosting_data'] = 'Hosting data';
$lang['hosting_list'] = 'Hosting list';
$lang['hosting_package'] = 'Hosting package';

$lang['domain'] = 'Domain name';
$lang['add_domain'] = 'Add new domain';
$lang['edit_domain'] = 'Edit domain';
$lang['domain_data'] = 'Domain data';
$lang['domain_list'] = 'Domains list';
$lang['main_domain'] = 'Primary domain';


$lang['users_list'] = "Users list";
$lang['add_user'] = 'Add new user';
$lang['user_data'] = 'User data';
$lang['user_settings'] = 'User settings';
$lang['user_group'] = 'User group';
$lang['user_active'] = 'User active';
$lang['user_login'] = 'User log in';
$lang['login'] = 'Log in';
$lang['logout'] = 'Log out';
$lang['wrong_login'] = "<strong>Warning!</strong> Worong data or no specific user in database!";


$lang['server_disk_info'] = 'Server disk info';
$lang['space_avelable'] = 'Avelable space: ';
$lang['space_in_use'] = 'Space in use:';

$lang['ftp_users'] = 'FTP users info';
$lang['recomended'] = 'Avelable number: ';
$lang['created'] = 'Created:';

$lang['db_info'] = 'Database info';
$lang['db_list'] = "Database list";
$lang['add_db'] = 'Add new database';
$lang['database_data'] = 'Database';
$lang['db_name'] = 'Databese name';
$lang['db_user'] = 'Databese user';
$lang['edit_db'] = 'Edit database';

$lang['trello_board'] = 'Trello board';
$lang['trello'] = "insert trello boart id";
$lang['tasks_admin'] = 'Users tasks';
$lang['tasks_list'] = 'Tasks list';
$lang['ctrello'] = 'Connect Trello';
$lang['dtrello'] = 'Disconnect Trello';


$lang['task_name'] = 'Tasks name';
$lang['task_desc'] = 'Tasks description';
$lang['task_link'] = 'Tasks link';
$lang['task_flags'] = 'Tasks flags';
$lang['task_status'] = 'Tasks status';

$lang['add_new_task'] = 'Add new task';


