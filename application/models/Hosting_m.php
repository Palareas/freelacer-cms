<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class  hosting_m  extends  MY_Model  {

    function __construct()
    {
        parent::__construct();
    }
    public function get_hosting_list($order_by = 'id', $order='ASC', $limit = FALSE , $start = 0){
	 	if($limit!=FALSE) {
	 		$this->db->limit($limit,$start);
	 	}
	 	$this->db->select('h.id as id, h.username, h.end as end, hp.name as package, d.name as domain')
	 		->from('hosting h')
	 		->join('hosting_packages as hp', 'h.package = hp.id')
	 		->join('domains as d', 'h.id = d.hosting_id')
	 		->where(array('d.main_domain' => 1))
	 		->order_by($order_by,$order);

	 	$query = $this->db->get();
	 	return $query->result_object();
	}
	
}