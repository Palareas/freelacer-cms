<?php 

class data_m extends MY_Model  {

    function __construct() {
        parent::__construct();
    }

	public function data_hosting(){
	 	$this->db->select('h.id, h.username, p.db_numb')
	 		->from('hosting h')
	 		->join('hosting_packages as p', 'h.package = p.id')
	 		//->join('data_hosting as d', 'h.id = d.hosting_id')
	 		->where(array('p.db_numb >' => '0'));
	 		// ->order_by($order_by,$order);
	 	$query = $this->db->get();
	 	return $query->result_object();
	}

	public function db_limit($id){
	 	$this->db->select('h.id, p.db_numb')
	 		->from('hosting h')
	 		->join('hosting_packages as p', 'h.package = p.id')
	 		->where(array('h.id' => $id));
	 	$query = $this->db->get();
	 	return $query->row_object();
	}

	public function db_number($id){
	 	$this->db->select('id')
	 		->from('data_hosting')
	 		->where(array('hosting_id' => $id));
	 	$query = $this->db->get();
	 	return $query->result_object();
	}
	public function users_db($id){

	 	$this->db->select('data.id, data.name, data.username')
	 		->from('data')
	 		->join('data_hosting', 'data.id = data_hosting.data_id')
	 		->where(array('data_hosting.hosting_id' => $id));

	 	$query = $this->db->get();
	 	return $query->result_object();
	 	
	}
}