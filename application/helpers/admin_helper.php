<?php 
function get_random_password($length = 10)
{
	$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	return $randomString;
}

function get_package()
{
	$CI =& get_instance();
	$CI->load->model('hosting_m');

	$packages =  $CI->hosting_m->select_from('*','hosting_packages');
	$options = array(''=>'---');
	foreach ($packages as $package) {
		$options[$package->id] = $package->name;
	}
	return $options;
}

function get_owner()
{
	$CI =& get_instance();
	$CI->load->model('customers_m');

	$owners =  $CI->hosting_m->select_from('id, owner_name, owner_surname','customers');
	$options = array(''=>'---');
	
	foreach ($owners as $owner) {
		$options[$owner->id] = $owner->owner_name . ' ' . $owner->owner_surname;
	}
	return $options;
}

function get_hosting()
{
	$CI =& get_instance();
	$CI->load->model('hosting_m');

	$hosting =  $CI->hosting_m->select_from('id, username','hosting');
	$options = array(''=>'---');
	
	foreach ($hosting as $ftp) {
		$options[$ftp->id] = $ftp->username;
	}
	return $options;
}

function get_user_group()
{
	$CI =& get_instance();
	$CI->load->model('user_m');

	$users =  $CI->user_m->select_from('id, name','users_group');
	$options = array(''=>'---');
	
	foreach ($users as $user) {
		$options[$user->id] = $user->name;
	}
	return $options;
}

function expire($end)
{
	$date = new DateTime();
	$end = new DateTime($end);

	$interval = $date->diff($end);
	//var_dump($interval);
	$days = $interval->format('%a');
	$class = FALSE;
	if($days <= 60 && $days >30) {
		$class = 'warning';
	}elseif($days <= 30){
		$class = 'danger';
	}
	return $class;

}

function get_menu_link($url)
{
	$link = '';

	if(isset($_GET['token'])) {
		$link = base_url("$url/?token=").$_GET['token'];
	}else {
		$link = base_url("$url");
	}


	return $link;
}

function menu_active($location)
{
	$CI =& get_instance();
	$CI->load->helper('url');
	$class = "";
	$current = $CI->uri->segment(1);

	if($current == $location){
		$class = 'active';
	}
	return $class;
}

function user_rules($rules) {
	
	$CI =& get_instance();
	$CI->load->model('user_m');
	$rules = explode(",", $rules);
	$group = $CI->session->userdata('group');
	
	if($group != NULL) {
		$users =  $CI->user_m->get_data('user_key', 'users_group', array('id'=>$group));
		foreach ($rules as $rule) {
			if($users->user_key === $rule) {		
				return TRUE;			
			}
		}	
	}
	return FALSE;
}




function get_disk_info($value){

	$disk_info['disk_size'] = getSize(disk_total_space("/"));
	$disk_info['free_space'] = getSize(disk_free_space("/"));
	$disk_info['in_use'] = $disk_info['disk_size'] - $disk_info['free_space'];
	$disk_info['percent'] = sprintf("%.2f",($disk_info['in_use'] * 100)/$disk_info['disk_size']);


	switch ($value) {
		case 'disk_size':
			return $disk_info['disk_size'] .' GB';
			break;
		case 'free_space':
			return $disk_info['free_space'] .' GB';
			break;
		case 'in_use':
			return $disk_info['in_use'] .' GB';
			break;
		case 'percent':
			return $disk_info['percent'] .'%';
			break;
		default:
			return $disk_info['disk_size'] .' GB';
			break;
	}
}

function getSize($bytes) {

		for($i = 0; $bytes >= 1024; $i++ ) {

			$bytes /= 1024;

		}
		return round($bytes, 1);
}

function get_disk_percent($used, $full){

	return sprintf("%.2f%s",(($used * 100)/$full) ,"%");
}


function db_limit($hosting_id, $check = FALSE)
{
	$CI =& get_instance();
	$CI->load->model('data_m');
	$db_number =  count($CI->data_m->db_number($hosting_id));
	$data_hosting = $CI->data_m->db_limit($hosting_id);
	if($check == TRUE) {
		if($db_number < $data_hosting->db_numb) {
			return TRUE;
		}else {
			return FALSE;
		}
	}
	return $db_number;
}

function db_hosting()
{
	$CI =& get_instance();
	$CI->load->model('hosting_m');
	$CI->load->model('data_m');

	$hosting =  $CI->hosting_m->select_from('id, username','hosting');
	$options = array(''=>'---');

	foreach ($hosting as $ftp) {

	$db_number =  count($CI->data_m->db_number($ftp->id));
	$data_hosting = $CI->data_m->db_limit($ftp->id);
	
		if($db_number < $data_hosting->db_numb) {
			$options[$ftp->id] = $ftp->username;
		}

	}
	return $options;
}
// function get_disk_percent(){
	
// 	$percent = (get_disk_in_use() * 100)/get_disk_size();
// 	return $percent;
// }


// function get_disk_size(){
// 	$disk_size = disk_total_space("/");

// 	return getSize($disk_size);
// }

// function get_disk_free(){
// 	$disk_free_space = disk_free_space("/");


// 	return getSize($disk_free_space);

// }

// function get_disk_in_use(){
// 	$disk_size = disk_total_space("/");
// 	$disk_free_space = disk_free_space("/");
// 	$disk_in_use = $disk_size - $disk_free_space;

// 	return getSize($disk_in_use);
// }

// function addUnits($bytes) {

// 		$units = array( 'B', 'KB', 'MB', 'GB', 'TB' );

// 		for($i = 0; $bytes >= 1024 && $i < count($units) - 1; $i++ ) {

// 			$bytes /= 1024;

// 		}
// 		return round($bytes, 1).' '.$units[$i];
// }